Pod::Spec.new do |s|
  s.name             = 'AddictiveLib'
  s.version          = '1.0.09'
  s.summary          = 'The spine of all applications.'

  s.description      = <<-DESC
  Contains basic management and utility files for quickly creating and organizing new applications
                       DESC

  s.homepage         = 'https://bitbucket.com/cdh-dev'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dylan Hanson' => 'cdh.create@gmail.com' }
  s.source           = { :git => 'https://cdh-dev@bitbucket.org/cdh-dev/addictivelib.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.swift_version = '4.1'

  s.source_files = 'AddictiveLib/Classes/**/*'

  s.dependency 'Unbox'
  s.dependency 'Alamofire'
  s.dependency 'UnboxedAlamofire'

end
